package ns.assessment.springboot2.springboot2swagger2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ns.assessment.springboot2.springboot2swagger2.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/v1")

public class LockerController{
    @Autowired
    private LockerRepository lockerRepository;

    //get all lockers
    @GetMapping("/lockers")
    public List<Locker> getAllLockers(){
        return lockerRepository.findAll();
    }

    //get one locker by id
    @GetMapping("/lockers/{readableId}")
    public ResponseEntity<Locker> getLockerById(@PathVariable(value = "readableId") Long lockerId) throws ResourceNotFoundException{
        Locker locker = lockerRepository.findById(lockerId).orElseThrow(() -> new ResourceNotFoundException ("Locker not found for this id ::" + lockerId));
        return ResponseEntity.ok().body(locker);
    }

    //create new locker
    @PostMapping("/lockers")
    public Locker createLocker(@Valid @RequestBody Locker locker){
        return lockerRepository.save(locker);
    }

    //update a locker
    @PutMapping("/lockers/{readableId}")
    public ResponseEntity<Locker> updateLocker(@PathVariable(value = "readableId") Long lockerId, @Valid @ RequestBody Locker lockerDetails) throws ResourceNotFoundException{
        Locker locker = lockerRepository.findById(lockerId).orElseThrow(() -> new ResourceNotFoundException ("Locker not found for this id ::" + lockerId));
        
        locker.setId(lockerDetails.getId());
        locker.setReadableId(lockerDetails.getReadbleId());
        locker.setStatus(lockerDetails.getStatus());
        locker.setReservedBy(lockerDetails.getReservedBy());
        final Locker updatedLocker = lockerRepository.save(locker);
        return ResponseEntity.ok(updatedLocker);
    }

    //delete a locker
    @DeleteMapping("/lockers/{readableId}")
    public Map<String, Boolean> deleteLocker(@PathVariable(value = "readableId") Long lockerId) throws ResourceNotFoundException{
        Locker locker = lockerRepository.findById(lockerId).orElseThrow(() -> new ResourceNotFoundException ("Locker not found for this id ::" + lockerId));

        lockerRepository.delete(locker);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    //add customer to a locker
    @PutMapping("/locker/{readableId}")
        public ResponseEntity<Locker> addCustomer(@PathVariable(value = "readableId") Long lockerId, @Valid @ RequestBody Locker lockerDetails) throws ResourceNotFoundException{
            Locker locker = lockerRepository.findById(lockerId).orElseThrow(() -> new ResourceNotFoundException ("Locker not found for this id ::" + lockerId));

            locker.setReservedBy(lockerDetails.getReservedBy());
            final Locker updatedLocker = lockerRepository.save(locker);
            return ResponseEntity.ok(updatedLocker);
    }
} 