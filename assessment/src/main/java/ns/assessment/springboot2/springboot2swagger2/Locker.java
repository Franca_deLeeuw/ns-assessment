package ns.assessment.springboot2.springboot2swagger2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Locker")
public class Locker {

    private String id;
    private String readableId;
    private String status;
    private String reservedBy;

    public Locker() {

    }

    public Locker(String id, String readableId, String status, String reservedBy) {
        this.id = id;
        this.readableId = readableId;
        this.status = status;
        this.reservedBy = reservedBy;
    }

    @Column(name = "id", nullable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "readableId", nullable = false)
    public String getReadbleId() {
        return readableId;
    }

    public void setReadableId(String readableId) {
        this.readableId = readableId;
    }

    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "reservedBy", nullable = true)
    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    @Override
    public String toString() {
        return "Locker [id=" + id + ", readableId=" + readableId + ", status=" + status + ", reservedBy=" + reservedBy
                + "]";
    }
}